function compare(a, b, key) {
  if (a[key] < b[key]) {
    return -1;
  }
  if (a[key] > b[key]) {
    return 1;
  }
  return 0;
}

function orderByKey(arr, key) {
  let tmp = [...arr];
  tmp.sort((a, b) => compare(a, b, key));
  return tmp;
}

function getRandomItem(items) {
  return items[(items.length * Math.random()) | 0];
}

function roll3d6() {
  const rolls = [
    Math.floor(Math.random() * (6 - 1 + 1) + 1),
    Math.floor(Math.random() * (6 - 1 + 1) + 1),
    Math.floor(Math.random() * (6 - 1 + 1) + 1),
    Math.floor(Math.random() * (6 - 1 + 1) + 1),
  ];
  return rolls.reduce((a, b) => a + b) - Math.min.apply(null, rolls);
}

function camalize(source) {
  return source
    .toLowerCase()
    .trim()
    .split(/[ \(\)]/g)
    .filter(Boolean)
    .reduce(
      (result, current) => result + current[0].toUpperCase() + current.slice(1)
    );
}

// https://tio.run/##pdG9bsMgEADg2TzFLZWM7CL/9EdK6hfo0AwdowyYYBfXgsomlSLLz@5CVKlVBoOd5RBwfHc6GvpNe9aJL30v1ZFPU3WSTAsl4chpG6qyiUHiAQUt16CggIEQYk5HFDAlew2f/Nyb413ZcKaJ3dlHeIuCSnUQ2mfC3Cdbs7yANEsUYTBgoPY2ey/g7oKQlstafxwOEBWQosBU6Lg@dRIUGhFCtpxqOWlVHb6@795Irzsha1Gdw0urA90kYwwJxlAUBVylDEA3kIwY@zrpjJNaxwuKS3dTMZgk8G3tV5xtb5WYucR0qZjPiNmf6E3GzG@YNjITrQ5Ldedg/@tLceeMbVyL5554ugZ/cP3lLfijE8/W40@eeLYGf57B82t8mn4A

function deal(obj, n) {
  let o = { ...obj };
  const keys = Object.keys(obj);
  for (let i = 0; i < n; i++) {
    o[keys[i % keys.length]] += 1;
  }
  return o;
}

function except(a, b) {
  const setA = new Set(a);
  const setB = new Set(b);
  return [...setA].filter((x) => !setB.has(x));
}

function capitalize(source) {
  return source[0].toUpperCase() + source.slice(1);
}

function flatDeep(arr, d = Infinity) {
  return d > 0
    ? arr.reduce(
        (acc, val) =>
          acc.concat(Array.isArray(val) ? flatDeep(val, d - 1) : val),
        []
      )
    : arr.slice();
}

function distinct(source){
  return [...new Set(source)];
}