const abilities = [
  "strength",
  "dexterity",
  "constitution",
  "intelligence",
  "wisdom",
  "charisma",
];

const alignments = [
  "lawfulGood",
  "neutralGood",
  "chaoticGood",
  "lawfulNeutral",
  "neutral",
  "chaoticNeutral",
];

const translation = {
  fr: {
    skills: {
      acrobatics: "Acrobaties",
      appraise: "Estimation",
      bluff: "Bluff",
      climb: "Escalade",
      craft: "Artisanat",
      diplomacy: "Diplomatie",
      disableDevice: "Sabotage",
      disguise: "Déguisement",
      escapeArtist: "Evasion",
      fly: "Vol",
      handleAnimal: "Dressage",
      heal: "Premier secours",
      intimidate: "Intimidation",
      knowledgeArcana: "Connaissances (mystères)",
      knowledgeDungeoneering: "Connaissances (exploration souterraine)",
      knowledgeEngineering: "Connaissances (ingénierie)",
      knowledgeGeography: "Connaissances (géographie)",
      knowledgeHistory: "Connaissances (histoire)",
      knowledgeLocal: "Connaissances (folklore local)",
      knowledgeNature: "Connaissances (nature)",
      knowledgeNobility: "Connaissances (noblesse)",
      knowledgePlanes: "Connaissances (plans)",
      knowledgeReligion: "Connaissances (religion)",
      linguistics: "Linguistique",
      perception: "Perception",
      perform: "Représentation",
      profession: "Profession",
      ride: "Équitation",
      senseMotive: "Psychologie",
      sleightOfHand: "Escamotage",
      spellcraft: "Art de la magie",
      stealth: "Discrétion",
      survival: "Survie",
      swim: "Natation",
      useMagicDevice: "Utilisation d'objets magiques",
    },
    abilities: {
      strength: "force",
      dexterity: "dextérité",
      constitution: "constitution",
      intelligence: "intelligence",
      wisdom: "sagesse",
      charisma: "charisme",
    },
    alignments: {
      lawfulGood: "Loyal Bon",
      neutralGood: "Neutre Bon",
      chaoticGood: "Chaotique Bon",
      lawfulNeutral: "Loyal Neutre",
      neutral: "Neutre",
      chaoticNeutral: "Chaotique Neutre",
    },
  },
};

const background = {
  patate: {
    strength: 10,
    dexterity: 10,
    constitution: 10,
    intelligence: 10,
    wisdom: 10,
    charisma: 10,
  },
  savage: {
    strength: 14,
    dexterity: 12,
    constitution: 14,
    intelligence: 10,
    wisdom: 16,
    charisma: 8,
  },
  violent: {
    strength: 16,
    dexterity: 14,
    constitution: 14,
    intelligence: 10,
    wisdom: 12,
    charisma: 8,
  },
  scholar: {
    strength: 8,
    dexterity: 14,
    constitution: 12,
    intelligence: 16,
    wisdom: 10,
    charisma: 14,
  },
  religious: {
    strength: 14,
    dexterity: 8,
    constitution: 14,
    intelligence: 10,
    wisdom: 16,
    charisma: 14,
  },
  resourceful: {
    strength: 10,
    dexterity: 14,
    constitution: 10,
    intelligence: 14,
    wisdom: 14,
    charisma: 14,
  },
  random: {
    strength: roll3d6(),
    dexterity: roll3d6(),
    constitution: roll3d6(),
    intelligence: roll3d6(),
    wisdom: roll3d6(),
    charisma: roll3d6(),
  },
  bestial: {
    strength: 16,
    dexterity: 12,
    constitution: 12,
    intelligence: 12,
    wisdom: 12,
    charisma: 12,
  },
  nifty: {
    strength: 12,
    dexterity: 12,
    constitution: 12,
    intelligence: 12,
    wisdom: 12,
    charisma: 16,
  },
  rustic: {
    strength: 14,
    dexterity: 14,
    constitution: 14,
    intelligence: 10,
    wisdom: 14,
    charisma: 10,
  },
};

const armorgroups = {
  light: {
    none: {
      cost: 0,
      bonus: 0,
      maximumDexBonus: 256,
      armorPenalty: 0,
      translation: "Tenue de voyage",
    },
    leather: {
      cost: 10,
      bonus: 2,
      maximumDexBonus: 6,
      armorPenalty: 0,
      translation: "Armure de cuir",
    },
    studdedLeather: {
      cost: 25,
      bonus: 3,
      maximumDexBonus: 5,
      armorPenalty: -1,
      translation: "Armure de cuir cloutée",
    }
  },
  medium: {
    hide: {
      cost: 15,
      bonus: 4,
      maximumDexBonus: 4,
      armorPenalty: -3,
      translation: "Armure en peau",
    },
    scaleMail: {
      cost: 50,
      bonus: 5,
      maximumDexBonus: 3,
      armorPenalty: -4,
      translation: "Armure d'écailles",
    },
  },
};

// todo: handle small size!
const weapongroups = {
  melee: {
    dagger: {
      damage: "1d3",
      damageSmall: "1d4",
      translation: "dague",
    },
    longsword: {
      damage: "1d8",
      damageSmall: "1d6",
      translation: "épée longue",
    },
    quarterstaff: {
      damage: "1d6/1d6",
      damageSmall: "1d4/1d4",
      translation: "bâton",
    },
    lightMace: {
      damage: "1d6",
      damageSmall: "1d4",
      translation: "Masse d'armes légère",
    },
  },
  ranged: {
    longbow: {
      damage: "1d8",
      damageSmall: "1d6",
      translation: "arc long",
      ammunition: "carquois de 20 flèches",
    },
    crossbowLight: {
      damage: "1d8",
      damageSmall: "1d6",
      translation: "arbalète légère",
      ammunition: "carquois de 20 carreaux",
    },
    axeThrowing: {
      damage: "1d6",
      damageSmall: "1d4",
      translation: "hache de jet",
      ammunition: "2 haches de jet",
    },
  },
};

const shields = {
  shieldLightWooden: {
    cost: 3,
    bonus: 1,
    maximumDexBonus: 256,
    armorPenalty: -1,
    translation: "Rondache (bois)",
  },
  shieldHeavyWooden: {
    cost: 7,
    bonus: 2,
    maximumDexBonus: 256,
    armorPenalty: -2,
    translation: "Écu (bois)",
  },
};

const allSkills = {
  acrobatics: {
    trained: false,
    armorPenalty: 1,
    ability: "dexterity",
    value: 0,
  },
  appraise: {
    trained: false,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  bluff: {
    trained: false,
    armorPenalty: 0,
    ability: "charisma",
    value: 0,
  },
  climb: {
    trained: false,
    armorPenalty: 1,
    ability: "strength",
    value: 0,
  },
  craft: {
    trained: false,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  diplomacy: {
    trained: false,
    armorPenalty: 0,
    ability: "charisma",
    value: 0,
  },
  disableDevice: {
    trained: true,
    armorPenalty: 1,
    ability: "dexterity",
    value: 0,
  },
  disguise: {
    trained: false,
    armorPenalty: 0,
    ability: "charisma",
    value: 0,
  },
  escapeArtist: {
    trained: false,
    armorPenalty: 1,
    ability: "dexterity",
    value: 0,
  },
  fly: {
    trained: false,
    armorPenalty: 1,
    ability: "dexterity",
    value: 0,
  },
  handleAnimal: {
    trained: true,
    armorPenalty: 0,
    ability: "charisma",
    value: 0,
  },
  heal: {
    trained: false,
    armorPenalty: 0,
    ability: "wisdom",
    value: 0,
  },
  intimidate: {
    trained: false,
    armorPenalty: 0,
    ability: "charisma",
    value: 0,
  },
  knowledgeArcana: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  knowledgeDungeoneering: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  knowledgeEngineering: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  knowledgeGeography: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  knowledgeHistory: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  knowledgeLocal: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  knowledgeNature: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  knowledgeNobility: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  knowledgePlanes: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  knowledgeReligion: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  linguistics: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  perception: {
    trained: false,
    armorPenalty: 0,
    ability: "wisdom",
    value: 0,
  },
  perform: {
    trained: false,
    armorPenalty: 0,
    ability: "charisma",
    value: 0,
  },
  profession: {
    trained: true,
    armorPenalty: 0,
    ability: "wisdom",
    value: 0,
  },
  ride: {
    trained: false,
    armorPenalty: 1,
    ability: "dexterity",
    value: 0,
  },
  senseMotive: {
    trained: false,
    armorPenalty: 0,
    ability: "wisdom",
    value: 0,
  },
  sleightOfHand: {
    trained: true,
    armorPenalty: 1,
    ability: "dexterity",
    value: 0,
  },
  spellcraft: {
    trained: true,
    armorPenalty: 0,
    ability: "intelligence",
    value: 0,
  },
  stealth: {
    trained: false,
    armorPenalty: 1,
    ability: "dexterity",
    value: 0,
  },
  survival: {
    trained: false,
    armorPenalty: 0,
    ability: "wisdom",
    value: 0,
  },
  swim: {
    trained: false,
    armorPenalty: 1,
    ability: "strength",
    value: 0,
  },
  useMagicDevice: {
    trained: true,
    armorPenalty: 0,
    ability: "charisma",
    value: 0,
  },
};

const classes = {
  rogue: {
    save: {
      fortitude: 0,
      reflex: 2,
      will: 0,
    },
    bba: 0,
    hitDice: 8,
    name: "roublard",
    skillRanks: 8,
    skills: [
      "perception",
      "stealth",
      "acrobatics",
      "knowledgeLocal",
      "climb",
      "sleightOfHand",
      "disableDevice",
      "escapeArtist",
      "disguise",
      "bluff",
      "diplomacy",
      "intimidate",
      "appraise",
      "senseMotive",
      "swim",
      "knowledgeDungeoneering",
      "useMagicDevice",
      "linguistics",
      "craft",
      "perform",
      "profession",
    ],
    armor: armorgroups.light.leather,
    melee: weapongroups.melee.dagger,
    //   ranged: weapongroups.ranged.crossbowLight, // pour matilin
    specialAttacks: "attaque sournoise +1d6",
    finesse: true,
    features: ["sens des pièges +1"],
    gear: ["outils de voleur", "trousse de déguisement"],
  },
  barbarian: {
    save: {
      fortitude: 2,
      reflex: 0,
      will: 0,
    },
    bba: 1,
    hitDice: 12,
    name: "barbare",
    skillRanks: 4,
    skills: [
      "perception",
      "intimidate",
      "survival",
      "climb",
      "ride",
      "handleAnimal",
      "knowledgeNature",
      "acrobatics",
      "swim",
      "craft",
    ],
    armor: armorgroups.medium.hide,
    melee: weapongroups.melee.longsword,
    ranged: weapongroups.ranged.longbow,
    specialAttacks: "rage (4 + Con fois par jour)",
    features: ["déplacement accéléré", "rage de berserker"],
    gear: ["des cailloux", "une amulette à l'effigie de son totem"],
  },
  druid: {
    save: {
      fortitude: 2,
      reflex: 0,
      will: 2,
    },
    bba: 0,
    hitDice: 8,
    name: "druide",
    skillRanks: 4,
    skills: [
      "knowledgeNature",
      "handle animal",
      "heal",
      "survival",
      "perception",
      "spellcraft",
      "climb",
      "ride",
      "swim",
      "fly",
      "knowledgeGeography",
      "craft",
      "profession",
    ],
    armor: armorgroups.medium.hide,
    language: "druidique",
    melee: weapongroups.melee.quarterstaff,
    features: [
      "pacte avec la nature",
      "instinct naturel",
      "oraisons",
      "empathie sauvage",
    ],
    gear: ["symbole sacré en bois", "sacoche à composantes"],
    spells: {
      level0: [
        {
          name: "création d’eau",
          description: "Crée huit litres d’eau pure/niveau",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Cr%c3%a9ation%20deau.ashx",
        },
        {
          name: "purification de nourriture et d’eau",
          description: "Purifie 30 dm³/niveau de nourriture et d’eau",
          link: "Purifie 30 dm³/niveau de nourriture et d’eau",
        },
        {
          name: "repérage",
          description: "Le PJ sait où se trouve le nord",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Rep%c3%a9rage.ashx",
        },
      ],
      // https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Caract%c3%a9ristiques.ashx#TABLEAUCARACTERISTIQUES
      level1: [
        {
          name: "gourdin magique",
          description:
            "Transforme bâton ou gourdin en arme +1 (2d6+1/2d6+1) pendant 1 minute/niveau.",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Gourdin%20magique.ashx",
        },
        {
          name: "soins légers",
          description: "Rend 1d8 pv au sujet, +1/niveau (max. +5)",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.soins%20l%c3%a9gers.ashx",
        },
      ],
    },
    mainAbility: "wis",
  },
  fighter: {
    save: {
      fortitude: 2,
      reflex: 0,
      will: 0,
    },
    bba: 1,
    hitDice: 10,
    name: "guerrier",
    skillRanks: 2,
    skills: [
      "ride",
      "climb",
      "survival",
      "intimidate",
      "swim",
      "handleAnimal",
      "knowledgeDungeoneering",
      "knowledgeEngineering",
      "profession",
      "craft",
    ],
    armor: armorgroups.medium.hide,
    shield: shields.shieldHeavyWooden,
    melee: weapongroups.melee.longsword,
    ranged: weapongroups.ranged.longbow,
    features: ["don supplémentaire"],
    gear: ["2 chausses-trappes"],
  },
  wizard: {
    save: {
      fortitude: 0,
      reflex: 0,
      will: 2,
    },
    bba: 0,
    hitDice: 6,
    name: "magicien",
    skillRanks: 2,
    skills: [
      "spellcraft",
      "knowledgeArcana",
      "knowledgePlanes",
      "knowledgeHistory",
      "knowledgeGeography",
      "knowledgeDungeoneering",
      "knowledgeEngineering",
      "knowledgeNature",
      "knowledgeNobility",
      "knowledgeLocal",
      "knowledgeReligion",
      "appraise",
      "fly",
      "linguistics",
      "craft",
      "profession",
    ],
    armor: armorgroups.light.none,
    melee: weapongroups.melee.quarterstaff,
    language: "draconien",
    features: [
      "pacte magique",
      "école de magie",
      "tours de magie",
      "écriture de parchemins",
    ],
    gear: ["grimoire", "sacoche à composantes"],
    spells: {
      level0: [
        {
          name: "détection de la magie",
          description: "Détecte sorts et objets magiques à 18 m à la ronde",
          link:
            "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.D%c3%a9tection%20de%20la%20magie.ashx",
        },
        {
          name: "rayon de givre",
          description: "Rayon infligeant 1d3 points de dégâts de froid",
          link:
            "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Rayon%20de%20givre.ashx",
        },
        {
          name: "manipulation à distance",
          description: "Télékinésie limitée (2,5 kg max.)",
          link:
            "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Manipulation%20%c3%a0%20distance.ashx",
        },
      ],
      // https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Caract%c3%a9ristiques.ashx#TABLEAUCARACTERISTIQUES
      level1: [
        {
          name: "charme-personne",
          description: "La cible devient l’ami du PJ.",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Charme-personne.ashx",
          save: "Vol",
        },
        {
          name: "couleurs dansantes",
          description:
            "Assomme, aveugle et/ou étourdit des créatures en fonction de leurs DV.",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.couleurs%20dansantes.ashx",
          save: "Vol",
        },
      ],
    },
    mainAbility: "int",
  },
  oracle: {
    save: {
      fortitude: 0,
      reflex: 0,
      will: 2,
    },
    bba: 0,
    hitDice: 8,
    name: "oracle",
    skillRanks: 4,
    skills: [
      "knowledgePlanes",
      "knowledgeReligion",
      "heal",
      "senseMotive",
      "spellcraft",
      "diplomacy",
      "knowledgeHistory",
      "craft",
      "profession",
    ],
    armor: armorgroups.light.leather,
    melee: weapongroups.melee.lightMace,
    features: ["malédiction de l'oracle", "mystère", "oraison", "révélation"],
    gear: [
      "livre sur la divination",
      "sacoche à composantes",
      "grand manteau à capuche",
    ],
    spells: {
      level0: [
        {
          name: "assistance divine",
          description:
            "+1 sur un jet d’attaque, un jet de sauvegarde ou un test de compétence",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Assistance%20divine.ashx",
        },
        {
          name: "création d’eau",
          description: "Crée huit litres d’eau pure/niveau",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Cr%c3%a9ation%20deau.ashx",
        },
        {
          name: "détection de la magie",
          description: "Détecte sorts et objets magiques à 18 m à la ronde",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.D%c3%a9tection%20de%20la%20magie.ashx",
        },
        {
          name: "lumière",
          description: "Fait briller un objet comme une torche",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Lumi%c3%a8re.ashx",
        },
      ],
      // https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Caract%c3%a9ristiques.ashx#TABLEAUCARACTERISTIQUES
      level1: [
        {
          name: "injonction",
          description: "La cible obéit à un ordre d’un mot pendant 1 round",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Injonction.ashx",
          save: "Vol",
        },
        {
          name: "compréhension des langages",
          description: "Le PJ comprend tous les langages écrits ou parlés",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Compr%c3%a9hension%20des%20langages.ashx",
        },
        {
          name: "soins légers",
          description: "Rend 1d8 pv au sujet, +1/niveau (max. +5)",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.soins%20l%c3%a9gers.ashx",
        },
      ],
    },
    spellsPerDay: {
      level1: 4,
    },
    mainAbility: "cha",
  },
  sorcerer: {
    save: {
      fortitude: 0,
      reflex: 0,
      will: 2,
    },
    bba: 0,
    hitDice: 6,
    name: "ensorceleur",
    skillRanks: 2,
    skills: [
      "knowledgeArcana",
      "spellcraft",
      "useMagicDevice",
      "bluff",
      "intimidate",
      "fly",
      "appraise",
      "craft",
      "profession",
    ],
    armor: armorgroups.light.none,
    melee: weapongroups.melee.dagger,
    ranged: weapongroups.ranged.crossbowLight,
    features: [
      {
        name: "pouvoir de lignage",
        link:
          "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.ensorceleur.ashx#LIGNAGE",
      },
      {
        name: "tours de magie",
        link:
          "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.ensorceleur.ashx#Tours_de_magie_3",
      },
      {
        name: "dispense de composantes",
        link:
          "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Dispense%20de%20composantes%20mat%c3%a9rielles.ashx",
      },
    ],
    gear: ["masque"],
    spells: {
      level0: [
        {
          name: "hébétement",
          description: "Fait perdre 1 action à un humanoïde de 4 DV ou moins",
          link:
            "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.H%c3%a9b%c3%a9tement.ashx",
          save: "Vol",
        },
        {
          name: "choc",
          description:
            "Une étincelle d’électricité frappe la cible, 1d3 points de dégâts d’électricité",
          link: "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Choc.ashx",
        },
        {
          name: "détection de la magie",
          description: "Détecte sorts et objets magiques à 18 m à la ronde",
          link:
            "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.D%c3%a9tection%20de%20la%20magie.ashx",
        },
        {
          name: "lecture de la magie",
          description: "Permet de lire parchemins et livres de sorts",
          link:
            "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Lecture%20de%20la%20magie.ashx",
        },
      ],
      level1: [
        {
          name: "couleurs dansantes",
          description:
            "Assomme, aveugle et/ou étourdit des créatures en fonction de leurs DV",
          link:
            "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Couleurs%20dansantes.ashx",
          save: "Vol",
        },
        {
          name: "projectile magique",
          description:
            "1d4+1 points de dégâts, +1 projectile tous les 2 niveaux au-delà de 1 (max. 5)",
          link:
            "https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Projectile%20magique.ashx",
        },
      ],
    },
    spellsPerDay: {
      level1: 4,
    },
    mainAbility: "cha",
  },
  paladin: {
    save: {
      fortitude: 2,
      reflex: 0,
      will: 2,
    },
    bba: 1,
    hitDice: 10,
    name: "paladin",
    skillRanks: 2,
    skills: [
      "diplomacy",
      "heal",
      "ride",
      "knowledgeNobility",
      "knowledgeReligion",
      "senseMotive",
      "handleAnimal",
      "spellcraft",
      "profession",
      "craft",
    ],
    armor: armorgroups.medium.scaleMail,
    shield: shields.shieldHeavyWooden,
    melee: weapongroups.melee.longsword,
    specialAttacks: "châtiment du mal 1/jour",
    features: ["aura du bien", "détection du mal"],
    gear: ["symbole sain en argent", "tabar"],
  },
  ranger: {
    save: {
      fortitude: 2,
      reflex: 2,
      will: 0,
    },
    bba: 1,
    hitDice: 10,
    name: "rôdeur",
    skillRanks: 6,
    skills: [
      "perception",
      "survival",
      "handleAnimal",
      "knowledgeNature",
      "ride",
      "stealth",
      "heal",
      "knowledgeGeography",
      "climb",
      "knowledgeDungeoneering",
      "intimidate",
      "swim",
      "spellcraft",
      "profession",
      "craft",
    ],
    armor: armorgroups.light.studdedLeather,
    shield: shields.shieldLightWooden,
    melee: weapongroups.melee.longsword,
    ranged: weapongroups.ranged.longbow,
    features: ["1er ennemi juré", "pistage", "empathie sauvage"],
    gear: ["sac de baie", "appeau", "collet"],
  },
};

const raceFeatures = {
  lowLightVision: {
    name: "vision nocturne",
    link:
      "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Cr%C3%A9ateur%20de%20race.ashx#SENS_41",
  },
  darkVision: {
    name: "vision dans le noir",
    link:
      "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Cr%C3%A9ateur%20de%20race.ashx#SENS_41",
  },
};

const races = {
  human: {
    name: "humain",
    hasBonusAbility: true,
    ability: {},
    features: ["compétent", "don en bonus"],
  },
  halfElf: {
    name: "demi-elfe",
    hasBonusAbility: true,
    ability: {
      charisma: 2,
    },
    language: "elfe",
    features: [raceFeatures.lowLightVision],
  },
  halfDrow: {
    name: "demi-drow",
    hasBonusAbility: true,
    ability: {
      intelligence: 2,
    },
    language: "drow",
    features: [raceFeatures.darkVision, "Aveuglé par la lumière"],
  },
  halfOrc: {
    name: "demi-orque",
    hasBonusAbility: true,
    ability: {},
    skills: {
      acrobatics: 1,
      climb: 1,
    },
    save: {
      fortitude: 1,
      reflex: 1,
      will: 1,
    },
    language: "orque",
    features: [raceFeatures.darkVision, "tatouage sacré"],
  },
  elf: {
    name: "elfe",
    ability: {
      dexterity: 2,
      constitution: -2,
      intelligence: 2,
    },
    skills: { perception: 2 },
    language: "elfe",
    features: [
      raceFeatures.lowLightVision,
      "porteur de lumière: lumière (à volonté)",
    ],
  },
  // Stoic Negotiator+Iron Citizen+Unstoppable
  dwarf: {
    name: "nain",
    ability: {
      constitution: 2,
      wisdom: 2,
      charisma: -2,
    },
    skills: {
      bluff: 2,
      diplomacy: 2,
      senseMotive: 2,
    },
    hitPoint: 3,
    features: [raceFeatures.darkVision],
    language: "nain",
  },
  tiefling: {
    name: "tieffelin",
    ability: {
      dexterity: 2,
      intelligence: 2,
      charisma: -2,
    },
    skills: {
      bluff: 2,
      stealth: 2,
    },
    features: [
      raceFeatures.darkVision,
      "pouvoir magique: ténèbres (1/j)",
      "magie des fiélons",
    ],
    language: getRandomItem(["abyssal", "infernal"]),
    resist: [
      { name: "froid", value: 5 },
      { name: "électricité", value: 5 },
      { name: "feu", value: 5 },
    ],
  },
  orc: {
    name: "orque",
    ability: {
      strength: 2,
      wisdom: 2,
      charisma: -2,
    },
    skills: {
      knowledgeNature: 2,
    },
    features: [raceFeatures.darkVision, "odorat"],
    language: "orque",
  },
  dryad: {
    name: "dryade",
    ability: {
      strength: -2,
      dexterity: 2,
      wisdom: 2,
    },
    skills: {
      knowledgeNature: 1,
      survival: 1,
    },
    ac: [
      {
        name: "arnure naturelle",
        bonus: 1,
        type: "a",
      },
    ],
    features: [
      raceFeatures.lowLightVision,
      {
        name: "communication avec les plantes",
        link:
          "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Cr%C3%A9ateur%20de%20race.ashx#AUTRES_TRAITS_RACIAUX_43",
      },
      {
        name: "camouflage (forêt)",
        link:
          "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Cr%C3%A9ateur%20de%20race.ashx#Traits_standard_4",
      },
      {
        name: "armure naturelle",
        link:
          "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Cr%C3%A9ateur%20de%20race.ashx#Traits_standard_1",
      },
    ],
    vulnerability: ["feu"],
    language: "sylvestre",
  },
  gnome: {
    name: "gnome",
    size: "P",
    ability: {
      strength: -2,
      constitution: 2,
      charisma: 2,
    },
    skills: {
      stealth: 4,
      perception: 2,
      disableDevice: 1,
      knowledgeEngineering: 1,
    },
    ac: [
      {
        name: "petite taille",
        bonus: 1,
      },
    ],
    attack: 1,
    bmo: -1,
    dmd: -1,
    language: ["gnome", "sylvestre"],
    features: [
      raceFeatures.lowLightVision,
      "magie gnome",
      "résistance aux illusions",
    ],
  },
  halfling: {
    name: "halfelin",
    size: "P",
    ability: {
      strength: -2,
      dexterity: 2,
      charisma: 2,
    },
    save: {
      fortitude: 1,
      reflex: 1,
      will: 1,
    },
    skills: {
      stealth: 4,
      perception: 2,
      acrobatics: 2,
      climb: 2,
    },
    ac: [
      {
        name: "petite taille",
        bonus: 1,
      },
    ],
    attack: 1,
    bmo: -1,
    dmd: -1,
    language: ["halfelin"],
    features: ["sans peur"],
  },
  aasimar: {
    name: "aasimar",
    ability: {
      wisdom: 2,
      charisma: 2,
    },
    skills: {
      perception: 2,
      diplomacy: 2,
    },
    language: ["céleste"],
    features: [
      raceFeatures.darkVision,
      "pouvoir magique: lumière du jour (1/j)",
    ],
    resist: [
      { name: "acide", value: 5 },
      { name: "froid", value: 5 },
      { name: "électricité", value: 5 },
    ],
  },
  goblin: {
    name: "gobelin",
    size: "P",
    ability: {
      strength: -2,
      dexterity: 4,
      charisma: -2,
    },
    skills: {
      stealth: 8,
      ride: 4,
    },
    ac: [
      {
        name: "petite taille",
        bonus: 1,
      },
    ],
    attack: 1,
    bmo: -1,
    dmd: -1,
    language: ["gobelin"],
    features: [raceFeatures.darkVision],
  },
};

// https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Vocabulaire%20courant.ashx
const abbreviations = {
  for: "Force",
  dex: "Dextérité",
  con: "Constitution",
  int: "Intelligence",
  sag: "Sagesse",
  char: "Charisme",
  vol: "Volonté",
  ref: "Réflexe",
  vig: "Vigueur",
  dd: "Degré de Difficulté",
};

// https://www.scriptorium.d100.fr/index.php/archives/jeux-heberges-2/microlite20/
const gears = [
  [
    "sac à dos",
    "sacoche de ceinture",
    "couverture",
    "lanterne à capote",
    "10 flasques d’huile",
    "silex et amorce",
    "pelle",
    "peigne",
    "gourde",
    "rations de voyage (4 jours)",
  ],
  [
    "sac à dos",
    "sacoche de ceinture",
    "couverture",
    "10 torches",
    "3 flasques d’huile",
    "10 pièces de craie",
    "sangle de 3m",
    "miroir",
    "pied-de-biche",
    "gourde d’eau",
    "rations de voyage (4 jours)",
  ],
  [
    "sac à dos",
    "sacoche de ceinture",
    "couverture",
    "tente",
    "10 torches",
    "5 flasques d’huile",
    "silex et amorce",
    "corde en chanvre de 15 m",
    "grappin",
    "1kg de farine",
    "rations de voyage (4 jours)",
  ],
];
