/*
scrap allskills: https://www.d20pfsrd.com/skills/
function camalize(source) {
        return source
          .toLowerCase()
          .trim()
          .split(/[ \(\)]/g)
          .filter(Boolean)
          .reduce(
            (result, current) =>
              result + current[0].toUpperCase() + current.slice(1)
          );
      }
function toAbility(mod) {
    switch(mod){
    case "Str": return "Strength";
    case "Dex": return "Dexterity";
    case "Con": return "Constitution";
    case "Int": return "Intelligence";
    case "Wis": return "Wisdom";
    case "Cha": return "Charisma";
    default: return "";
    }
}

copy(Object.fromEntries(new Map([...document.querySelectorAll('.article-content > div:nth-child(5) > table:nth-child(1) > tbody:nth-child(3) > tr')].map(x => [[camalize(x.querySelector('td').textContent.trim())], {"trained": x.querySelector("td:nth-child(21)").textContent.trim() !== "Yes", "armorPenalty": x.querySelector("td:nth-child(22)").textContent.trim() === "Yes" ? -1 : 0, "ability": toAbility(x.querySelector("td:nth-child(23)").textContent.trim()), "value": 0 }]))))
*/

function sumSkill(key, classSkills, raceSkills, ability, armorPenalty, skill) {
  const canUse =
    classSkills[key] !== undefined ||
    (raceSkills && raceSkills[key] !== undefined) ||
    !skill.trained;
  if (!canUse) return { total: null, detail: "" };

  const totalClassBonus =
    (classSkills[key] ?? 0) > 0 ? classSkills[key] + 3 : 0;
  const totalRaceBonus = (raceSkills && raceSkills[key]) ?? 0;
  const totalArmorPenalty = armorPenalty * (skill?.armorPenalty ?? 0);
  const totalAbilityBonus = canUse ? ability[skill.ability] : 0;
  return {
    total:
      totalClassBonus + totalRaceBonus + totalAbilityBonus + totalArmorPenalty,
    detail: `class: ${totalClassBonus} + race: ${totalRaceBonus} + ability: ${totalAbilityBonus} + armor: ${totalArmorPenalty}`,
  };
}

// sumSkill("a", {"a": 2},{"a": 2},{"w": 3}, 2, {"a": {armorPenalty:1, ability: "w"}})
function sumSkills(clasSkills, raceSkills, ability, armorPenalty) {
  const skills = [];
  for (let key of Object.keys(allSkills)) {
    const { total, detail } = sumSkill(
      key,
      clasSkills,
      raceSkills,
      ability,
      armorPenalty,
      allSkills[key]
    );
    if (total === null) continue;

    skills.push({
      total: total,
      detail: detail,
      key: key,
      translation: translateSkill(key),
    });
  }
  return skills;
}

function loadQueryString() {
  const data = {
    vocation: "barbarian",
    race: "human",
    backgroundAbility: "patate",
    title: "Karaktero",
  };
  const search = window.location.search;
  if (!search) return data;

  if (search.toLowerCase().includes("katell"))
    return {
      vocation: "wizard",
      race: "elf",
      backgroundAbility: "scholar",
      title: "Asara",
    };

  if (search.toLowerCase().includes("matilin"))
    return {
      vocation: "rogue",
      race: "tiefling",
      backgroundAbility: "nifty",
      title: "Bi Ryung",
    };

  if (search.toLowerCase().includes("enora"))
    return {
      vocation: "druid",
      race: "orc",
      backgroundAbility: "rustic",
      title: "Enora",
    };

  if (search.toLowerCase().includes("kim"))
    return {
      vocation: "rogue",
      race: "dwarf",
      backgroundAbility: "resourceful",
      title: "Ygrune",
    };

  return data;

  // const parameters = [...(new URLSearchParams(search)).entries()];
  // if (parameters.length == 0)
  //     return data

  // const validParameters =
  //     parameters
  //     .filter(([key, value]) => !!key && !!value && parseFloat(value).toString() === value)
  // if (validParameters.length == 0)
  //     return data

  // const results =
  //     Object.fromEntries(
  //         validParameters.map(([key, value]) => [key, parseFloat(value)]));

  // for (const key of Object.keys(data).filter(key => results[key] != undefined)) {
  //     data[key] = results[key]
  // }

  // return data
}

function translateSkill(skill) {
  return translation["fr"].skills[skill];
}

function translateAbility(ability) {
  return translation["fr"].abilities[ability];
}

function translateAlignment(alignment) {
  return translation["fr"].alignments[alignment];
}

new Vue({
  el: "#app",
  data: {
    translation: translation,
    abilities: abilities,
    alignments: alignments,
    armorgroups: armorgroups,
    classes: classes,
    races: races,
    gears: gears,
    level: 1,
    alignment: "neutral",
    raceAbilityBonus: "strength",
    ...loadQueryString(),
  },
  computed: {},
  methods: {
    sumAbility: function (ability) {
      if (
        this.races[this.race].hasBonusAbility &&
        this.raceAbilityBonus === ability
      ) {
        return background[this.backgroundAbility][ability] + 2;
      }

      return (
        background[this.backgroundAbility][ability] +
        (this.races[this.race].ability[ability] ?? 0)
      );
    },
    strength: function () {
      return this.sumAbility("strength");
    },
    dexterity: function () {
      return this.sumAbility("dexterity");
    },
    constitution: function () {
      return this.sumAbility("constitution");
    },
    intelligence: function () {
      return this.sumAbility("intelligence");
    },
    wisdom: function () {
      return this.sumAbility("wisdom");
    },
    charisma: function () {
      return this.sumAbility("charisma");
    },
    str: function () {
      return this.mod(this.strength());
    },
    dex: function () {
      return this.mod(this.dexterity());
    },
    con: function () {
      return this.mod(this.constitution());
    },
    int: function () {
      return this.mod(this.intelligence());
    },
    wis: function () {
      return this.mod(this.wisdom());
    },
    cha: function () {
      return this.mod(this.charisma());
    },
    mod: function (ability) {
      return Math.floor((ability - 10) / 2);
    },
    dmd: function () {
      return (
        10 +
        this.str() +
        this.dex() +
        classes[this.vocation].bba +
        (races[this.race].dmd ?? 0)
      );
    },
    bmo: function () {
      return (
        this.str() + classes[this.vocation].bba + (races[this.race].bmo ?? 0)
      );
    },
    ac: function (armor, shield) {
      return (
        10 +
        this.dex() +
        armor +
        shield +
        (races[this.race].ac?.map((x) => x.bonus).reduce((a, b) => a + b, 0) ??
          0)
      );
    },
    touch: function () {
      return (
        10 +
        this.dex() +
        (races[this.race].ac
          ?.filter((x) => x.type === undefined || x.type === "b")
          .map((x) => x.bonus)
          .reduce((a, b) => a + b, 0) ?? 0)
      );
    },
    flatFooted: function (armor, shield) {
      return (
        10 +
        armor +
        shield +
        (races[this.race].ac
          ?.filter((x) => x.type === undefined || x.type === "a")
          .map((x) => x.bonus)
          .reduce((a, b) => a + b, 0) ?? 0)
      );
    },
    sumSave: function (ability, save) {
      return (
        ability +
        classes[this.vocation].save[save] +
        (races[this.race].save && races[this.race].save[save]
          ? races[this.race].save[save]
          : 0)
      );
    },
    fortitude: function () {
      return this.sumSave(this.con(), "fortitude");
    },
    reflex: function () {
      return this.sumSave(this.dex(), "reflex");
    },
    will: function () {
      return this.sumSave(this.wis(), "will");
    },
    hp: function (vocation) {
      return (
        this.con() +
        classes[vocation].hitDice +
        (races[this.race].hitPoint ?? 0)
      );
    },
    //   skillRanks: function () {
    //     return this.int() + classes[vocation].skillRanks;
    //   },
    skills: function () {
      const vocation = this.classes[this.vocation];
      const classesSkills = vocation.skills;

      // let obj = {};
      // for (skill of classesSkills)
      //     obj[skill] = 0;

      const obj = Object.fromEntries(new Map(classesSkills.map((s) => [s, 0])));

      const classesSkillsDealed = deal(obj, this.int() + vocation.skillRanks);
      const ability = {
        strength: this.str(),
        dexterity: this.dex(),
        constitution: this.con(),
        intelligence: this.int(),
        wisdom: this.wis(),
        charisma: this.cha(),
      };
      const penalty =
        vocation.armor.armorPenalty + (vocation.shield?.armorPenalty ?? 0);
      return sumSkills(
        classesSkillsDealed,
        this.races[this.race].skills,
        ability,
        penalty
      );
    },
    translateSkill: translateSkill,
    translateAbility: translateAbility,
    translateAlignment: translateAlignment,
    buildSkillHref: function (skill) {
      var skillWithoutParenthesis = skill.replace(/\(.*\)/g, "").trim();
      return `https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.${skillWithoutParenthesis}.ashx`;
    },
    forceSymbol: function (number) {
      return number >= 0 ? `+${number}` : number.toString();
    },
    languages: function () {
      const raceLanguages = flatDeep([this.races[this.race].language]);
      return distinct(
        ["commun"]
          .concat(raceLanguages)
          .concat([this.classes[this.vocation].language])
          .filter(Boolean)
      );
    },
    dd: function (spellLevel) {
      return (
        10 +
        spellLevel +
        (this.classes[this.vocation].mainAbility
          ? this[this.classes[this.vocation].mainAbility]()
          : 0)
      );
    },
    abbr: function (term) {
      return abbreviations[term.toLowerCase()];
    },
    concentration: function () {
      return (
        this.level +
        (this.classes[this.vocation].mainAbility
          ? this[this.classes[this.vocation].mainAbility]()
          : 0)
      );
    },
    orderByKey: orderByKey,
    getRandomItem: getRandomItem,
    except: except,
    capitalize: capitalize,
    gear: function () {
      const package = getRandomItem(this.gears);
      return classes[this.vocation].gear.concat(package).join(", ");
    },
    features: function () {
      return this.races[this.race].features.concat(
        this.classes[this.vocation].features
      );
    },
    meleeAttack: function () {
      const str = this.str();
      const vocation = classes[this.vocation];
      const name = vocation.melee.translation;
      const attack =
        (vocation.finesse ? this.dex() : str) +
        vocation.bba +
        (races[this.race].attack ?? 0);
      const damage = races[this.race].size === 'P' ? vocation.melee.damageSmall : vocation.melee.damage;
      const strToDamage = str != 0 ? " " + this.forceSymbol(str) : "";
      return `${name}, ${this.forceSymbol(attack)} (${damage}${strToDamage})`;
    },
    rangedAttack: function () {
      const vocation = classes[this.vocation];
      const name = vocation.ranged.translation;
      const attack = this.dex() + vocation.bba + (races[this.race].attack ?? 0);
      const damage =  races[this.race].size === 'P' ? vocation.ranged.damageSmall : vocation.ranged.damage;
      return `${name}, ${this.forceSymbol(attack)} (${damage})`;
    },
  },
});
